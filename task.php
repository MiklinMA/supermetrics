<?php

declare(strict_types = 1);

define('API_URL', "https://api.supermetrics.com/assignment");
define('CLIENT_ID', "ju16a6m81mhid5ue1z3v2g0uh");

class API {
    private function parse_response($result) {
        if(!$result){die("Connection Failure");}
        $result = json_decode($result, true);

        if (array_key_exists('error', $result)) {
            $message = $result['error'];
            $message = array_key_exists('message', $message) ? $message['message'] : $message;
            die($message);
        }

        return $result['data'];
    }
    public function call($method, $url, $data, $mh=null) {
        if (gettype($url) == 'array') {
            $mh = curl_multi_init();

            $curls = array();
            foreach ($url as $k => $u) {
                $d = $data ? $data[$k] : null;
                $curls[$k] = $this->call($method, $u, $d, $mh);
            }

            $running = null;
            do {
                curl_multi_exec($mh, $running);
            } while($running > 0);

            $result = array();
            foreach($curls as $id => $curl) {
                $r = $this->parse_response(curl_multi_getcontent($curl));
                array_push($result, $r);

                curl_multi_remove_handle($mh, $curl);
            }

            curl_multi_close($mh);

            return $result;
        }

        $curl = curl_init();

        switch ($method){
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data) {
                if (gettype($data) == 'array')
                    $data = json_encode($data);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        curl_setopt($curl, CURLOPT_URL, API_URL.$url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if ($mh) {
            curl_multi_add_handle($mh, $curl);
            return $curl;
        } else {
            $result = curl_exec($curl);
            curl_close($curl);
            return $this->parse_response($result);
        }
    }

    public function post($url, $data) {
        return $this->call('POST', $url, $data);
    }
    public function get($url, $data) {
        return $this->call('GET', $url, $data);
    }
}

class Supermetrics {
    private $client_id;
    private $token;
    private $token_file = '/tmp/sm-token.txt';
    private $data_file = '/tmp/sm-data.json';
    private $api;
    private $posts;

    public $email;
    public $name;
    public $limit = 1000;
    public $use_cache;

    public function __construct($client_id, $email, $name, $use_cache=false) {
        $this->client_id = $client_id;
        $this->email = $email;
        $this->name = $name;
        $this->use_cache = $use_cache;
        $this->api = new API();

        if ($this->use_cache and file_exists($this->token_file)) {
            $this->token = file_get_contents($this->token_file);
        }
    }

    private function register() {
        $data_array = array(
            "client_id" => $this->client_id,
            "email" => $this->email,
            "name" => $this->name,
        );
        $response = $this->api->post('/register', $data_array);
        $this->token = $response['sl_token'];
        echo 'Token registered', PHP_EOL;

        if ($this->use_cache)
            file_put_contents($this->token_file, $this->token);
    }
    private function get_posts() {
        if (!$this->token) $this->register();

        if ($this->use_cache and file_exists($this->data_file)) {
            $this->posts = json_decode(file_get_contents($this->data_file));
        }
        if ($this->posts) return;

        $params = array(
            "sl_token" => $this->token,
        );
        $posts = array();
        $urls = array();
        $params = array();
        for ($page = 1; $page * 100 <= $this->limit; $page++) {
            array_push($params, array(
                "sl_token" => $this->token,
                "page" => $page,
            ));
            array_push($urls, '/posts');
        }
        $responses = $this->api->get($urls, $params);

        foreach($responses as $response) {
            $posts = array_merge($posts, $response['posts']);
        }

        $this->posts = json_encode($posts);

        if ($this->use_cache)
            file_put_contents($this->data_file, $this->posts);

        $this->posts = json_decode($this->posts);
        echo 'Posts fetched', PHP_EOL;
    }
    /* Show stats on the following:
     * - Average character length of a post / month
     * - Longest post by character length / month
     * - Total posts split by week
     * - Average number of posts per user / month
     */
    public function stats() {
        if (!$this->posts) $this->get_posts();

        $monthly = array();
        $weekly = array();
        $userly = array();

        foreach($this->posts as $post) {
            $length = strlen($post->message);
            $month = date('Y-m', strtotime($post->created_time));
            $week = date('W', strtotime($post->created_time));
            $user = $post->from_name;

            if (!array_key_exists($month, $monthly))
                $monthly[$month] = array();
            array_push($monthly[$month], $length);

            if (!array_key_exists($week, $weekly))
                $weekly[$week] = 0;
            $weekly[$week] ++;

            if (!array_key_exists($user, $userly))
                $userly[$user] = array();
                if (!array_key_exists($month, $userly[$user]))
                    $userly[$user][$month] = 0;

            $userly[$user][$month] ++;

        }
        $m_average = array();
        $m_longest = array();
        foreach($monthly as $key => $month) {
            $m_average[$key] = array_sum($month) / count($month);
            $m_longest[$key] = max($month);
        }

        foreach($userly as $key => $user) {
            $userly[$key] = array_sum($user) / count($user);
        }

        return array(
            'average_by_month' => $m_average,
            'longest_by_month' => $m_longest,
            'count_by_week' => $weekly,
            'average_by_user' => $userly,
        );
    }
}

$sm = new Supermetrics(CLIENT_ID,
    "MiklinMA@gmail.com",
    "Mike Miklin",
    false // use_cache
);
$stats = $sm->stats();
print(json_encode($stats, JSON_PRETTY_PRINT));

?>

